package com.example.exchangerates.architecture;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.example.exchangerates.di.dagger.ViewModelKey;
import com.example.exchangerates.ui.fragment.date.DateListViewModel;
import com.example.exchangerates.ui.fragment.exchange.ExchangeViewModel;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

@Module
public interface ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(DateListViewModel.class)
    ViewModel bindDateListViewModel(DateListViewModel viewModel);

    @Binds
    @IntoMap
    @ViewModelKey(ExchangeViewModel.class)
    ViewModel bindExchangeViewModel(ExchangeViewModel viewModel);

    @Binds
    ViewModelProvider.Factory bindViewModelFactory(ViewModelFactory factory);
}
