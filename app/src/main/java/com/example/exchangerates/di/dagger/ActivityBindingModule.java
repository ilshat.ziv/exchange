package com.example.exchangerates.di.dagger;

import com.example.exchangerates.ui.activity.exchange.ExchangeRatesActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public
interface ActivityBindingModule {

    @ActivityScope
    @ContributesAndroidInjector(modules = {
            ExchangeRatesBindingModule.class
    })
    ExchangeRatesActivity contributesExchangeRatesActivity();
}