package com.example.exchangerates.di.dagger;

import com.example.exchangerates.ui.fragment.date.DateListFragment;
import com.example.exchangerates.ui.fragment.exchange.ExchangeFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public interface ExchangeRatesBindingModule {

    @FragmentScope
    @ContributesAndroidInjector
    DateListFragment provideDateListFragment();

    @FragmentScope
    @ContributesAndroidInjector
    ExchangeFragment provideExchangeFragment();
}
