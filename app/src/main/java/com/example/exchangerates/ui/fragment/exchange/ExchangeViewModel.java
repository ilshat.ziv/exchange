package com.example.exchangerates.ui.fragment.exchange;

import androidx.annotation.NonNull;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.MutableLiveData;

import com.example.exchangerates.architecture.BaseViewModel;
import com.example.exchangerates.repository.exchange.IExchangeRepository;
import com.example.exchangerates.ui.fragment.exchange.model.ExchangeModel;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class ExchangeViewModel extends BaseViewModel {

    private final IExchangeRepository mIExchangeRepository;
    private DisposableSingleObserver mDisposableSingleObserver;

    private final MutableLiveData<ExchangeModel> mExchangeLiveData = new MutableLiveData<>();

    @Inject
    ExchangeViewModel(IExchangeRepository iExchangeRepository) {
        super();
        mIExchangeRepository = iExchangeRepository;
    }

    @Override
    public void subscribeToLiveData(@NonNull LifecycleOwner owner) {
        super.subscribeToLiveData(owner);
        mExchangeLiveData.observe(owner, dateModels -> isShowProgressLiveData.setValue(false));
    }

    MutableLiveData<ExchangeModel> getExchangeLiveData() {
        return mExchangeLiveData;
    }

    void bindData(@NonNull String date) {
        if (mDisposableSingleObserver != null && !mDisposableSingleObserver.isDisposed()) return;

        isShowProgressLiveData.setValue(true);

        mDisposableSingleObserver = mIExchangeRepository.getExchange(date)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<ExchangeModel>() {
                    @Override
                    public void onSuccess(ExchangeModel data) {
                        mDisposableSingleObserver.dispose();
                        mExchangeLiveData.setValue(data);
                    }

                    @Override
                    public void onError(Throwable e) {
                        mDisposableSingleObserver.dispose();
                        showError(e);
                    }
                });

        mCompositeDisposable.add(mDisposableSingleObserver);
    }
}
