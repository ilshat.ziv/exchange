package com.example.exchangerates.ui.fragment.exchange.model;

import androidx.annotation.NonNull;

import java.util.List;
import java.util.Objects;

public class ExchangeModel {

    private final String mDate;
    private final List<CurrencyModel> mCurrencyList;

    public ExchangeModel(@NonNull String date, @NonNull List<CurrencyModel> currencyList) {
        mDate = date;
        mCurrencyList = currencyList;
    }

    public String getDate() {
        return mDate;
    }

    public List<CurrencyModel> getCurrencyList() {
        return mCurrencyList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ExchangeModel that = (ExchangeModel) o;
        return Objects.equals(mDate, that.mDate) &&
                Objects.equals(mCurrencyList, that.mCurrencyList);
    }

    @Override
    public int hashCode() {
        return Objects.hash(mDate, mCurrencyList);
    }
}
