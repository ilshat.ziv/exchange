package com.example.exchangerates.ui.fragment.exchange.model;

import com.example.exchangerates.network.data.Currency;

import java.util.Objects;

public class CurrencyModel {

    private final String mName;
    private final String mCodeName;
    private final String mRate;

    private CurrencyModel(String name, String codeName, String rate) {
        mName = name;
        mCodeName = codeName;
        mRate = rate;
    }

    public CurrencyModel(Currency c) {
        this(c.getName(), c.getCharCode(), c.getValue());
    }

    public String getRate() {
        return String.valueOf(mRate);
    }

    public String getName() {
        return mName;
    }

    public String getCodeName() {
        return mCodeName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CurrencyModel that = (CurrencyModel) o;
        return Objects.equals(mCodeName, that.mCodeName) &&
                Objects.equals(mRate, that.mRate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(mCodeName, mRate);
    }
}
