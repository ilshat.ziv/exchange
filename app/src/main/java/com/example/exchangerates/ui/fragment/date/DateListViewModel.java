package com.example.exchangerates.ui.fragment.date;

import androidx.annotation.NonNull;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.MutableLiveData;

import com.example.exchangerates.architecture.BaseViewModel;
import com.example.exchangerates.ui.fragment.date.model.DateModel;
import com.example.exchangerates.utils.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class DateListViewModel extends BaseViewModel {

    private final MutableLiveData<List<DateModel>> mDateListLiveData = new MutableLiveData<>();
    private DisposableSingleObserver mDisposableSingleObserver;

    @Inject
    DateListViewModel() {
        super();
    }

    @Override
    public void subscribeToLiveData(@NonNull LifecycleOwner owner) {
        super.subscribeToLiveData(owner);
        mDateListLiveData.observe(owner, dateModels -> isShowProgressLiveData.setValue(false));
    }

    MutableLiveData<List<DateModel>> getDateListLiveData() {
        return mDateListLiveData;
    }

    void appendDate() {
        if (mDisposableSingleObserver != null && !mDisposableSingleObserver.isDisposed()) return;

        isShowProgressLiveData.setValue(true);

        mDisposableSingleObserver = Single.just(mDateListLiveData)
                .map(d -> {
                    List<DateModel> list = d.getValue();
                    if (CollectionUtils.isEmpty(list)) {
                        list = new ArrayList<>();
                        list.add(new DateModel(System.currentTimeMillis()));
                    }

                    long lastDateTime = list.get(CollectionUtils.size(list) - 1).getDate();

                    int i = 0;
                    while (i < 30) {
                        i++;
                        lastDateTime += TimeUnit.DAYS.toMillis(1);
                        list.add(new DateModel(lastDateTime));
                    }

                    return list;
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<List<DateModel>>() {
                    @Override
                    public void onSuccess(List<DateModel> dateModels) {
                        mDisposableSingleObserver.dispose();
                        mDateListLiveData.setValue(dateModels);
                    }

                    @Override
                    public void onError(Throwable e) {
                        mDisposableSingleObserver.dispose();
                        showError(e);
                    }
                });

        mCompositeDisposable.add(mDisposableSingleObserver);
    }
}
