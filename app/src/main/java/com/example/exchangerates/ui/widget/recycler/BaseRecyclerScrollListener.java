package com.example.exchangerates.ui.widget.recycler;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class BaseRecyclerScrollListener extends RecyclerView.OnScrollListener {

    private final OnRecyclerScrollListener mOnBottomScrollListener;
    private int mOldLastVisibleItem;

    public BaseRecyclerScrollListener(@NonNull OnRecyclerScrollListener listener) {
        super();
        mOnBottomScrollListener = listener;
    }

    @Override
    public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
    }

    @Override
    public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int scrollY) {
        if (scrollY > 0) {
            RecyclerView.LayoutManager layoutManager = recyclerView.getLayoutManager();
            if (layoutManager == null) {
                return;
            }
            int lastVisibleItem = getLastVisibleItem(recyclerView);
            if (lastVisibleItem == mOldLastVisibleItem) {
                return;
            }

            if (lastVisibleItem >= layoutManager.getItemCount() - 1) {
                if (mOnBottomScrollListener != null) {
                    mOnBottomScrollListener.onBottomScrolled();
                }
            }
            mOldLastVisibleItem = lastVisibleItem;
        }
    }

    private int getLastVisibleItem(@NonNull RecyclerView recyclerView) {
        RecyclerView.LayoutManager layoutManager = recyclerView.getLayoutManager();
        if (layoutManager instanceof LinearLayoutManager) {
            return ((LinearLayoutManager) layoutManager).findLastCompletelyVisibleItemPosition();
        }

        return 0;
    }

    public interface OnRecyclerScrollListener {
        void onBottomScrolled();
    }
}