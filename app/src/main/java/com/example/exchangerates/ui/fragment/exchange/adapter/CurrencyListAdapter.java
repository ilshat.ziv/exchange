package com.example.exchangerates.ui.fragment.exchange.adapter;

import com.example.exchangerates.R;
import com.example.exchangerates.ui.binding.BaseRecyclerAdapter;
import com.example.exchangerates.ui.fragment.exchange.model.CurrencyModel;

public class CurrencyListAdapter extends BaseRecyclerAdapter<CurrencyModel, BaseRecyclerAdapter.ViewHolder> {

    public CurrencyListAdapter() {
        super(R.layout.item_currency_list);
    }
}
