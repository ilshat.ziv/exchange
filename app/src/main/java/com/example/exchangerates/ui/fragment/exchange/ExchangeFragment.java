package com.example.exchangerates.ui.fragment.exchange;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.RecyclerView;

import com.example.exchangerates.R;
import com.example.exchangerates.databinding.FrDateListBinding;
import com.example.exchangerates.ui.activity.BaseActivity;
import com.example.exchangerates.ui.fragment.BaseFragment;
import com.example.exchangerates.ui.widget.refresh.RefreshView;
import com.example.exchangerates.ui.fragment.exchange.adapter.CurrencyListAdapter;
import com.example.exchangerates.ui.widget.toolbar.IToolbarController;

public class ExchangeFragment extends BaseFragment<FrDateListBinding, ExchangeViewModel> {

    public static final String DATE_EXCHANGE = "DATE_EXCHANGE";

    private RefreshView mRefreshView;
    private CurrencyListAdapter mCurrencyListAdapter;

    @Override
    protected int getLayoutId() {
        return R.layout.fr_exchange;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initViewModel();
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupUI();
    }

    private void setupUI() {
        initToolbar();
        mRefreshView = requireView().findViewById(R.id.swipeRefreshView);
        mRefreshView.setOnRefreshListener(() -> getViewModel().bindData(getDate()));
        initExchangeList();
    }

    private void initToolbar() {
        BaseActivity activity = (BaseActivity) requireActivity();
        IToolbarController iToolbarController = activity.getIToolbarController();
        iToolbarController.setTitle(R.string.app_rates_list_title);
    }

    private void initExchangeList() {
        RecyclerView recyclerView = requireView().findViewById(R.id.recyclerView);
        DividerItemDecoration itemDecor = new DividerItemDecoration(requireContext(), DividerItemDecoration.VERTICAL);
        itemDecor.setDrawable(getResources().getDrawable(R.drawable.item_decor));
        recyclerView.addItemDecoration(itemDecor);

        mCurrencyListAdapter = new CurrencyListAdapter();
        recyclerView.setAdapter(mCurrencyListAdapter);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        bind();
    }

    private void bind() {
        getViewModel().subscribeToLiveData(this);
        getViewModel().getExchangeLiveData().observe(getViewLifecycleOwner(),
                data -> {
                    BaseActivity activity = (BaseActivity) requireActivity();
                    IToolbarController iToolbarController = activity.getIToolbarController();
                    iToolbarController.setTitle(data.getDate());
                    mCurrencyListAdapter.replaceDataSet(data.getCurrencyList());
                }
        );
        getViewModel().bindData(getDate());
    }

    private String getDate() {
        Bundle arguments = getArguments();
        String defaultValue = String.valueOf(System.currentTimeMillis());
        return (arguments != null) ? arguments.getString(DATE_EXCHANGE, defaultValue) : defaultValue;
    }

    @Override
    protected void onShowProgressView() {
        mRefreshView.show();
    }

    @Override
    protected void onHideProgressView() {
        mRefreshView.hide();
    }
}