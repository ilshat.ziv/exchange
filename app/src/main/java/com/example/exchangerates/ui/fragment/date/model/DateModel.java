package com.example.exchangerates.ui.fragment.date.model;

import com.example.exchangerates.utils.DateUtil;

import java.util.Objects;

public class DateModel {

    private final long mDateTime;

    public DateModel(long dateTime) {
        mDateTime = dateTime;
    }

    public String getDateString() {
        return DateUtil.formatDataString(mDateTime, DateUtil.Format.EXCHANGE_FORMAT);
    }

    public long getDate() {
        return mDateTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DateModel dateModel = (DateModel) o;
        return mDateTime == dateModel.mDateTime;
    }

    @Override
    public int hashCode() {
        return Objects.hash(mDateTime);
    }
}
