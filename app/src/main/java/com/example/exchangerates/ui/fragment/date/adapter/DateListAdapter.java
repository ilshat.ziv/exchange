package com.example.exchangerates.ui.fragment.date.adapter;

import androidx.annotation.NonNull;
import androidx.databinding.ViewDataBinding;

import com.example.exchangerates.R;
import com.example.exchangerates.databinding.ItemDateListBinding;
import com.example.exchangerates.ui.binding.BaseRecyclerAdapter;
import com.example.exchangerates.ui.fragment.date.model.DateModel;

public class DateListAdapter extends BaseRecyclerAdapter<DateModel, DateListAdapter.ViewHolder> {

    private final OnDateListListener mOnCategoryItemListener;

    public DateListAdapter(@NonNull OnDateListListener listener) {
        super(R.layout.item_date_list);
        mOnCategoryItemListener = listener;
    }

    @Override
    protected ViewHolder buildViewHolder(ViewDataBinding binding) {
        return new ViewHolder(binding);
    }

    class ViewHolder extends BaseRecyclerAdapter.ViewHolder {

        ViewHolder(ViewDataBinding binding) {
            super(binding);
            ((ItemDateListBinding) binding).setListener(mOnCategoryItemListener);
        }
    }

    public interface OnDateListListener {
        void onDateListItemClick(@NonNull DateModel model);
    }
}