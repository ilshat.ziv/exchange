package com.example.exchangerates.ui.widget.toolbar;

import androidx.annotation.NonNull;
import androidx.annotation.StringRes;

public interface IToolbarController {

    void setTitle(@StringRes int title);

    void setTitle(@NonNull String title);
}