package com.example.exchangerates.ui.fragment.date;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.RecyclerView;

import com.example.exchangerates.R;
import com.example.exchangerates.databinding.FrDateListBinding;
import com.example.exchangerates.ui.activity.BaseActivity;
import com.example.exchangerates.ui.fragment.BaseFragment;
import com.example.exchangerates.ui.fragment.date.adapter.DateListAdapter;
import com.example.exchangerates.ui.fragment.exchange.ExchangeFragment;
import com.example.exchangerates.ui.widget.recycler.BaseRecyclerScrollListener;
import com.example.exchangerates.ui.widget.refresh.RefreshView;
import com.example.exchangerates.ui.widget.toolbar.IToolbarController;

public class DateListFragment extends BaseFragment<FrDateListBinding, DateListViewModel> {

    private RefreshView mRefreshView;
    private DateListAdapter mDateListAdapter;

    @Override
    protected int getLayoutId() {
        return R.layout.fr_date_list;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initViewModel();
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupUI();
    }

    private void setupUI() {
        initToolbar();
        mRefreshView = requireView().findViewById(R.id.swipeRefreshView);
        mRefreshView.setEnabled(false);
        initDateList();
    }

    private void initToolbar() {
        BaseActivity activity = (BaseActivity) requireActivity();
        IToolbarController iToolbarController = activity.getIToolbarController();
        iToolbarController.setTitle(R.string.app_date_list_title);
    }

    private void initDateList() {
        RecyclerView recyclerView = requireView().findViewById(R.id.recyclerView);
        DividerItemDecoration itemDecor = new DividerItemDecoration(requireContext(), DividerItemDecoration.VERTICAL);
        itemDecor.setDrawable(getResources().getDrawable(R.drawable.item_decor));
        recyclerView.addItemDecoration(itemDecor);
        recyclerView.addOnScrollListener(new BaseRecyclerScrollListener(() ->
                getViewModel().appendDate()
        ));
        mDateListAdapter = new DateListAdapter(model -> {
            NavController navController = Navigation.findNavController(requireView());
            Bundle bundle = new Bundle();
            bundle.putString(ExchangeFragment.DATE_EXCHANGE, model.getDateString());
            navController.navigate(R.id.fr_rate_list, bundle);
        });
        recyclerView.setAdapter(mDateListAdapter);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        bind();
    }

    private void bind() {
        getViewModel().subscribeToLiveData(this);
        getViewModel().getDateListLiveData().observe(getViewLifecycleOwner(),
                list -> mDateListAdapter.replaceDataSet(list)
        );
        getViewModel().appendDate();
    }

    @Override
    protected void onShowProgressView() {
        mRefreshView.show();
    }

    @Override
    protected void onHideProgressView() {
        mRefreshView.hide();
    }
}