package com.example.exchangerates.ui.activity.exchange;

import android.os.Bundle;

import androidx.annotation.Nullable;

import com.example.exchangerates.R;
import com.example.exchangerates.ui.activity.BaseActivity;

public class ExchangeRatesActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ac_exchange_rates);
    }
}
