package com.example.exchangerates.network.data;

import androidx.annotation.NonNull;

import com.example.exchangerates.utils.CollectionUtils;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.ArrayList;
import java.util.List;

@Root(name = "ValCurs", strict = false)
public class ExchangeResponse {

    @Attribute(name = "Date", required = false)
    private String mDate;

    @ElementList(entry = "Valute", required = false, inline = true)
    private List<Currency> mCurrencyList;

    public String getDate() {
        return mDate;
    }

    @NonNull
    public List<Currency> getCurrencyList() {
        return CollectionUtils.isEmpty(mCurrencyList) ? new ArrayList<>() : mCurrencyList;
    }
}