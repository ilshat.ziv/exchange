package com.example.exchangerates.network.client;

import com.example.exchangerates.network.data.ExchangeResponse;

import io.reactivex.Single;

import retrofit2.http.GET;
import retrofit2.http.Query;

public interface IExchangeClient {

    @GET("/scripts/XML_daily.asp")
    Single<ExchangeResponse> getExchange(@Query("date_req") String date);
}
