package com.example.exchangerates.network;

import com.example.exchangerates.BuildConfig;
import com.example.exchangerates.network.client.IExchangeClient;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

import okhttp3.OkHttpClient;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.simplexml.SimpleXmlConverterFactory;

@Module
public class NetworkModule {

    @Provides
    @Singleton
    IExchangeClient getIExchangeClient(OkHttpClient client) {
        return new Retrofit.Builder()
                .baseUrl(BuildConfig.EXCHANGE_RATES_HOST)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(SimpleXmlConverterFactory.create())
                .client(client)
                .build()
                .create(IExchangeClient.class);
    }

    @Provides
    @Singleton
    OkHttpClient getHttpClient() {
        return new OkHttpClient.Builder()
                .readTimeout(10, TimeUnit.SECONDS)
                .writeTimeout(10, TimeUnit.SECONDS)
                .connectTimeout(10, TimeUnit.SECONDS)
                .build();

    }
}