package com.example.exchangerates.network.data;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

import java.util.Objects;

@Root(name = "Valute", strict = false)
public class Currency {

    @Element(name = "CharCode")
    private String mCharCode;

    @Element(name = "Name")
    private String mName;

    @Element(name = "Value")
    private String mValue;

    public String getCharCode() {
        return mCharCode;
    }

    public String getName() {
        return mName;
    }

    public String getValue() {
        return mValue;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Currency currency = (Currency) o;
        return Objects.equals(mCharCode, currency.mCharCode) &&
                Objects.equals(mValue, currency.mValue);
    }

    @Override
    public int hashCode() {
        return Objects.hash(mCharCode, mValue);
    }
}
