package com.example.exchangerates.application;

import android.app.Application;

import com.example.exchangerates.architecture.ViewModelModule;
import com.example.exchangerates.di.dagger.ActivityBindingModule;
import com.example.exchangerates.network.NetworkModule;
import com.example.exchangerates.repository.RepositoryModule;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;

@Singleton
@Component(modules = {
        AndroidSupportInjectionModule.class,
        NetworkModule.class,
        RepositoryModule.class,
        ViewModelModule.class,
        ActivityBindingModule.class,
        CommonModule.class,
        NetworkModule.class
})
public interface AppComponent extends AndroidInjector<AppDelegate> {

    @Component.Builder
    interface Builder {

        @BindsInstance
        Builder application(Application application);

        Builder network(NetworkModule network);

        Builder context(CommonModule contextModule);

        AppComponent build();
    }
}