package com.example.exchangerates.application;

import com.example.exchangerates.network.NetworkModule;

import dagger.android.AndroidInjector;
import dagger.android.support.DaggerApplication;

public class AppDelegate extends DaggerApplication {

    @Override
    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        CommonModule commonModule = new CommonModule(this);
        return DaggerAppComponent.builder()
                .application(this)
                .context(commonModule)
                .network(new NetworkModule())
                .build();
    }
}
