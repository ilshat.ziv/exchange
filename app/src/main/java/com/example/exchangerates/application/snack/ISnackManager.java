package com.example.exchangerates.application.snack;

import androidx.annotation.NonNull;

public interface ISnackManager {

    void showLong(@NonNull TypeMessage type, @NonNull String message);
}
