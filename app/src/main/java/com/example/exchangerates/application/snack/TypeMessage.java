package com.example.exchangerates.application.snack;

public enum TypeMessage {
    Normal, Success, Info, Warning, Error
}