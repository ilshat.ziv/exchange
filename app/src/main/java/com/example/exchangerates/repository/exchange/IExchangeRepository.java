package com.example.exchangerates.repository.exchange;

import androidx.annotation.NonNull;

import com.example.exchangerates.ui.fragment.exchange.model.ExchangeModel;

import io.reactivex.Single;

public interface IExchangeRepository {

    Single<ExchangeModel> getExchange(@NonNull String date);
}