package com.example.exchangerates.repository;

import com.example.exchangerates.network.client.IExchangeClient;
import com.example.exchangerates.repository.exchange.ExchangeRepository;
import com.example.exchangerates.repository.exchange.IExchangeRepository;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class RepositoryModule {

    @Singleton
    @Provides
    IExchangeRepository provideIExchangeRepository (IExchangeClient client) {
        return new ExchangeRepository(client);
    }
}
