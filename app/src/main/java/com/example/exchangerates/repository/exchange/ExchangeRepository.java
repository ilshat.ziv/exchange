package com.example.exchangerates.repository.exchange;

import androidx.annotation.NonNull;

import com.example.exchangerates.network.client.IExchangeClient;
import com.example.exchangerates.ui.fragment.exchange.model.CurrencyModel;
import com.example.exchangerates.ui.fragment.exchange.model.ExchangeModel;

import java.util.List;

import io.reactivex.Single;

public class ExchangeRepository implements IExchangeRepository {

    private final IExchangeClient mIExchangeClient;

    public ExchangeRepository(@NonNull IExchangeClient IExchangeClient) {
        mIExchangeClient = IExchangeClient;
    }

    private Single<ExchangeModel> refreshExchange(@NonNull String date) {
        return mIExchangeClient.getExchange(date)
                .flatMap(r -> {
                    Single<List<CurrencyModel>> s1 = Single.just(r.getCurrencyList())
                            .toObservable()
                            .flatMapIterable(list -> list)
                            .map(CurrencyModel::new)
                            .toList();

                    Single<String> s2 = Single.just(r.getDate());
                    return Single.zip(s1, s2, (currencyModelList, s) ->
                            new ExchangeModel(s, currencyModelList)
                    );
                });
    }

    @Override
    public Single<ExchangeModel> getExchange(@NonNull String date) {
        return refreshExchange(date);
    }
}
